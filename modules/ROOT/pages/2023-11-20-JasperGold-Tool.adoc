---
title: Notes10/15 - 11/20 - JasperGold tutorials - Assertions Into JasperGold 

date: 2023-11-20 14:00:00 -0400

---

---
This post is my research for JasperGold the tool, with assertions working in tandum. 
_These notes were submitted on 2/7/24_

---

== JasperGold Assertions Tutorials and research (11/20 - 12/28)
* Summary of Last Notes
    ** Last notes gave tips and basics of how SVA Assertions worked.
    ** These notes will discuss how these will be used in JasperGold Specifically

* Jaspergold Assertions
    ** JasperGold has 12 tools to place RTL and SVA in
    ** SVA formating in Jasper
        *** Innefficent SVA
            **** Ex: Saying at A the same cycle GNT must be true
        *** Better SVA
            **** Ex: Saying at the _rising_ edge of A the same cycle GNT must be true
    ** Formal is all about efficency
        *** Innefficency Leads to:
            **** Hard to debug code
            **** Penalization
            **** High Memory Consumption
            **** Long Run Times
    ** Jaspergold nuances
        *** One such Nuance is A[*] and A[+]
            **** A[*]
                ***** O to infinite
            **** A[+]
                ***** 1 to infinite
            **** These are the same in modern Jaspergold, But will fail if older than version 2005
        *** Liveness Properties
            **** Jasper has difficulties with this in formal
            **** Use alternatives

* JasperGold Formal Analysis
    ** Formal
        *** Based on Formal Logic and mathematical proofs
        *** No assumptions and guess work
    ** Formal Examples
        *** Logical Equivalency Checking
        *** Model Checking
        *** Theorem Checking
    ** Terminology
        *** Invariant
            **** Boolean condition must always hold
        *** Temporal Sequence
            **** Condition evaluated over time
        *** Property
            **** Design Behavior by temporal and logical relationships
            **** Not the same as assertion, definition of behavior
        *** Assertion
            **** Verification directive
    ** Assumptions vs Asserts
        *** Assumptions
            **** Inputs
        *** Assertions
            **** Outputs
    ** Proofs
        *** Proven Unreachable Proof
            **** Verified to where the assert is true, but impossible to test through the entire cover, but it is true
        *** CEX Cover
            **** It completely hits the full cover but it hits an assertion failure
* Jasper Gold DLFA Flow
    ** Jaspergold is only one piece of the full verification cycle
    ** Jaspergold technically can not verificy if its correct, as they can only do what it is told
    ** If it does say incorrect, then it is invalid
    ** Check Correctness of Assumptions
        *** Assume-Guarantee Reasoning
        *** Simulation
    ** DLFA Designers and FVE
        *** Designers write assertions along RTL
        *** FV starts verification before testbench is ready
            **** At this point, don't verify entire testbench or corner cases
        *** Do FA alongside RTL designer for best outcome and efficency
    ** Main Steps
        *** Write RTL
        *** Identify focus areas for FA based on expected ROI
        ***  Write assertions and assumptions
	    *** Run FA tool
        *** Debug Failures
        *** Repeat cycle
    ** FV Focus Areas
        *** Arbiters
        *** Control Modules
        *** ECC
        *** Areas that look like bugs can easily pop up
        *** Small blks
* JasperGold Handoff Process / JasperGold Startup Tips
    ** Capture all debug information into one file called _filename.dho_
    ** In Formal Environments
        *** If there is a counter example, something is wrong
        *** Pick appropriate focus areas
        *** *Don't Overconstrain*
        *** Start with easy assumptions first
        *** Ideally, less constraints the better
        *** Do Sanity Checks
            **** Generally in code, if it seems obvious that it would have an assertion make sure it does
            **** Generally the obvious one are the one that aren't checked in the code
                ***** People _assume_ it is already checked
        *** Note:
            **** Mistakes in assumption coding can lead to overconstraining

            
