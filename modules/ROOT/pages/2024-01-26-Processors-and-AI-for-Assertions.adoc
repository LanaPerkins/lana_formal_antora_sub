---

title: Notes22/24 - 1/26 - Proccessors, Assertions, and AI

date: 2024-01-26 14:00:00 -0400

---

---

This Post is my research for a research paper for comparing two equivalent processors in Jaspergold with assertions using AI. Below will post the processors that was settled upon after much research and some notes on the current standing of AI assertions

_These notes were submitted on 2/8/24_

---

== Processors and AI (01/26, 01/29, 02/04)
* Processor Type
    ** The team chose to work with **RISC-V Single Cycle RV32I Core**
        *** Easy to understand
        *** Easy to find spec information
        *** Easy to Debug
    ** Planning to do System Verilog types
* Processors Chosen
    ** https://github.com/defermelowie/RV32I-course-project[RV32I-course-project]
        *** Meant for learning purposes
        *** Has been able to work in Jaspergold (go to Michael's page for more info)
    ** https://github.com/aytung94/DV_S2QED_RISCV[DV_S2QED_RISCV]
        *** Already has Assertions and TCL File In Jaspergold
            **** As the team is new, having a baseline from someone already doing Assertions can give more confedience in evaluating LLM and tool assertion output
        *** Has the entire spec and documentation to fall the work
        *** Spec
            **** https://github.com/onchipuis/mriscvcore[mriscvcore]
* Current AI Research Findings
    ** https://dl.acm.org/doi/abs/10.1145/3617555.3617874[GPT-3.5 Research]
        *** Uses GPT-3.5-turbo
        *** FormAI dataset
        *** Shows there are issues currently with this
        *** This is a very popular dataset, FormAI
    ** https://www.techopedia.com/verified-ai-the-way-forward-to-more-reliable-and-trustworthy-ai-systems#:~:text=Verified%20AI%20uses%20formal%20verification,%2C%20namely%2C%20specification%20and%20verification[Verified AI Paper]
        *** Verified AI uses formal verification techniques to provide mathematical proof of correctnes     - 
    ** https://arxiv.org/abs/2309.09437[GPT4ALL Verification]
        *** Bingo
        *** This is exactly what I was looking for
        *** Uses GPT4- generated SVA
        *** This was made in september, so it should be better than what the paper states
        *** This chose to have a empty testbench for a RTL module
    ** https://arxiv.org/abs/2401.03038[Verification and AI]
        *** This one was made on the 5th this year
        *** Not for formal verification specifically
    ** https://arxiv.org/pdf/2311.03739.pdf[AI and Formal Verification]
        *** Uses formal verification
    ** General Knowledge
        *** A researcher did research last summer on findings, and back then assertions weren't accurate
        *** Current Research showing LLama and GPT-4 Turbo runs more accurate assertions
        
== Plan For Future Research
* Look into current tools that would be best for inserting LLMS and tools around it
    ** Examples are Rift, TurboPilot, Private GPT
* When a tool and LLM is found, test its capabilities



            




            
